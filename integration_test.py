# pylint: disable=missing-class-docstring
# pylint: disable=missing-function-docstring
# pylint: disable=duplicate-code

"""
Integration test's module
"""

import contextlib
import io
import os
import tempfile
import unittest

import control_unit
import translator


class TestModel(unittest.TestCase):

    def assert_equal_result(self, source: str, code_target: str,
                            data_target: str, result_filename: str):
        translator.main([source, code_target, data_target])
        with contextlib.redirect_stdout(io.StringIO()) as stdout:
            control_unit.main([code_target, data_target])
        with open(result_filename, encoding="utf-8") as file:
            right_result = file.read()
        self.assertEqual(stdout.getvalue(), right_result)

    def test_cat(self):
        with tempfile.TemporaryDirectory() as tmpdir:
            source: str = "test/in/test_cat.asm"
            result_filename: str = "test/out/test_cat_result"
            code_target: str = os.path.join(tmpdir, "cat_code")
            data_target: str = os.path.join(tmpdir, "cat_data")
            self.assert_equal_result(source, code_target, data_target, result_filename)

    def test_hello(self):
        with tempfile.TemporaryDirectory() as tmpdir:
            source: str = "test/in/test_hello.asm"
            result_filename: str = "test/out/test_hello_result"
            code_target: str = os.path.join(tmpdir, "hello_code")
            data_target: str = os.path.join(tmpdir, "hello_data")
            self.assert_equal_result(source, code_target, data_target, result_filename)

    def test_prob1(self):
        with tempfile.TemporaryDirectory() as tmpdir:
            source: str = "test/in/test_prob1.asm"
            result_filename: str = "test/out/test_prob1_result"
            code_target: str = os.path.join(tmpdir, "prob1_code")
            data_target: str = os.path.join(tmpdir, "prob1_data")
            self.assert_equal_result(source, code_target, data_target, result_filename)
