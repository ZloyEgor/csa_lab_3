# pylint: disable=missing-class-docstring
# pylint: disable=missing-function-docstring
# pylint: disable=too-many-locals
# pylint: disable=too-many-branches
# pylint: disable=too-many-statements

"""
Module of translation .asm code into binary code and data files
"""

import sys
import logging

from tools import Command, twos_complement, WordWidth, AddrModeMask, AddrWidth, AddrMode

logging.basicConfig(level=logging.INFO)

symbol2command = {
    "halt": Command.HALT,
    "add": Command.ADD,
    "ld": Command.LD,
    "inc": Command.INC,
    "dec": Command.DEC,
    "cmp": Command.CMP,
    "jump": Command.JUMP,
    "beq": Command.BEQ,
    "bne": Command.BNE,
    "bmi": Command.BMI,
    "bpl": Command.BPL,
    "cout": Command.COUT,
    "out": Command.OUT,
    "st": Command.ST,
    "sub": Command.SUB,
    "mul": Command.MUL
}


def gel_labels(source_code: list):
    operation_counter: int = 0
    code_segment: bool = False
    labels: dict = {}

    for line in source_code:
        words = line.split()
        if len(words) == 0:
            continue
        if words[0] == '.code':
            code_segment = True
            continue
        if code_segment:
            if words[0] == '' or words[0][0] == ';':
                continue
            if words[0][-1] == ':':
                label_name: str = words[0][:-1]
                assert label_name not in labels, f"Label {label_name} already defined"
                labels[label_name] = operation_counter
                words.pop(0)
            if len(words) > 0 and words[0].lower() in symbol2command:
                operation_counter += 1

        else:
            continue
    return labels


def translate(source):
    with open(source, "r", encoding="utf-8") as source_file:
        source_code = source_file.readlines()
        data_segment = code_segment = False

        data: list = []
        code: list = []
        mnemonics: list = []
        variables: dict = {}

        labels: dict = gel_labels(source_code)

        if 'start' in labels.items():
            labels = {x: labels[x] + 1 for x in labels.items()}
            code.append(
                (symbol2command['jump'].code << AddrModeMask.bit_length()) + labels['start']
            )
            mnemonics.append('jump' + " " + str(labels['start']))

        for line in source_code:
            words = line.split()
            for value in words:
                if value == '.data':
                    assert not data_segment, "Data segment already defined"
                    data_segment = True
                    continue
                if value == '.code':
                    assert not code_segment, "Code segment already defined"
                    data_segment = False
                    code_segment = True
                    continue
                if value[0][0] == ';':
                    continue

            if data_segment:
                if len(words) >= 2 and words[0][-1] == ":":
                    var_name = words[0][:-1]
                    var_value = words[1]
                    assert var_name not in variables, \
                        f"Variable with name {var_name} already defined"
                    try:
                        var_value = int(var_value)
                    except ValueError as exc:
                        if len(var_value) == 1:
                            var_value = ord(var_value)
                        else:
                            raise ValueError(f"Invalid variable value: {var_name}") from exc

                    var_value = int(var_value)
                    data.append(var_value)
                    variables[var_name] = len(data) - 1
                elif len(words) == 1:
                    if words[0][0] != ';' and words[0] != '.data':
                        value = words[0]
                        try:
                            value = int(value)
                        except ValueError as exc:
                            if len(value) == 1:
                                value = ord(value)
                            else:
                                raise ValueError(f"Invalid data value: {value}") from exc
                        data.append(value)

            if code_segment:

                if len(words) == 0 or words[0] == '' or words[0][0] == ';' or words[0] == '.code':
                    continue

                if words[0][-1] == ':':
                    words.pop(0)
                    if len(words) == 0:
                        continue

                mnemonic: str = words[0].lower()

                if symbol2command[mnemonic].need_argument:
                    assert len(words) > 1, f"Command {mnemonic} requires argument"
                    argument: str = words[1]

                    if argument[0] == '#':
                        argument = argument[1:]
                        try:
                            value = int(argument)
                            code.append(
                                (symbol2command[mnemonic].code << AddrModeMask.bit_length()) + value
                            )
                            continue
                        except ValueError:
                            pass

                        assert argument in variables, f"Unknown variable name: {argument}"
                        address: int = variables[argument] + AddrMode.STRAIGHT_ABSOLUTE

                    elif argument[0] == '$':
                        argument = argument[1:]
                        try:
                            argument_value: int = int(argument)
                            address: int = argument_value
                            assert address <= (1 << AddrWidth), "Address overflow"
                            address += AddrMode.STRAIGHT_ABSOLUTE
                        except ValueError:
                            assert argument in variables, f"Unknown variable name: {argument}"
                            address: int = variables[argument] + AddrMode.INDIRECT_ABSOLUTE

                    elif argument in labels:
                        address: int = labels[argument]

                    elif argument in variables:
                        address: int = int(variables[argument]) + AddrMode.ADDR_REFERENCE

                    code.append(
                        (symbol2command[mnemonic].code << AddrModeMask.bit_length()) + address
                    )
                    mnemonics.append(mnemonic + " " + str(address))
                else:
                    code.append(symbol2command[mnemonic].code << AddrModeMask.bit_length())
                    mnemonics.append(mnemonic)

        return code, data, mnemonics


def write_bin(filename: str, data_list: list):
    with open(filename, "w", encoding="utf-8") as binary_file:
        for line in data_list:
            # print(twos_complement(line)[2:].zfill(12))
            binary_file.write(twos_complement(line)[2:].zfill(WordWidth))


def read_bin(filename: str):
    with open(filename, "r", encoding="utf-8") as binary_file:
        text = binary_file.read()
    return [text[i:i + WordWidth] for i in range(0, len(text), WordWidth)]


def main(args):
    assert len(args) == 3, \
        "Wrong arguments: translator.py <input_file> <code_target_file> <data_target_file>"
    source, code_target, data_target = args
    code, data, mnemonics = translate(source)

    write_bin(code_target, code)
    write_bin(data_target, data)

    for line in mnemonics:
        logging.info(line)


if __name__ == '__main__':
    main(sys.argv[1:])
