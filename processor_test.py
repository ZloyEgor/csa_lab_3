# pylint: disable=missing-class-docstring
# pylint: disable=missing-function-docstring
# pylint: disable=duplicate-code

"""
Test processor module on three algorithms: cat, hello, prob1
"""

import contextlib
import io
import unittest

import control_unit


class TestProcessor(unittest.TestCase):

    def assert_equal_output(self, code_target: str, data_target: str, result_filename: str):
        with open(result_filename, encoding="utf-8") as right_result_file:
            right_result = right_result_file.read()
        with contextlib.redirect_stdout(io.StringIO()) as stdout:
            control_unit.main([code_target, data_target])
        self.assertEqual(stdout.getvalue(), right_result)

    def test_cat_processing(self):
        code_target: str = "test/out/test_cat_code"
        data_target: str = "test/out/test_cat_data"
        result_filename: str = "test/out/test_cat_result"
        self.assert_equal_output(code_target, data_target, result_filename)

    def test_hello_processing(self):
        code_target: str = "test/out/test_hello_code"
        data_target: str = "test/out/test_hello_data"
        result_filename: str = "test/out/test_hello_result"
        self.assert_equal_output(code_target, data_target, result_filename)

    def test_prob1_processing(self):
        code_target: str = "test/out/test_prob1_code"
        data_target: str = "test/out/test_prob1_data"
        result_filename: str = "test/out/test_prob1_result"
        self.assert_equal_output(code_target, data_target, result_filename)
