# pylint: disable=missing-class-docstring
# pylint: disable=missing-function-docstring
# pylint: disable=too-few-public-methods

"""
Common tools with command descriptions, bit-masks and enums
"""

from enum import Enum, IntEnum

CommandCodeMask: int = 0b111100000000000000000000
AddressMask: int = 0b000000111111111111111111
AddrModeMask: int = 0b000011000000000000000000
WordWidth: int = 24
AddrWidth: int = 18
AddressAmount: int = 2 ** 20


class Operation:
    def __init__(self, code: int, need_argument: bool):
        self.code: int = code
        self.need_argument: bool = need_argument


class Command(Operation, Enum):
    ADD = 0b0001, True
    LD = 0b0010, True
    INC = 0b0011, False
    DEC = 0b0100, False
    CMP = 0b0101, True
    JUMP = 0b0111, True
    BEQ = 0b1000, True
    BNE = 0b1001, True
    BMI = 0b1010, True
    BPL = 0b1011, True
    OUT = 0b1100, False
    COUT = 0b1101, False
    ST = 0b1110, True
    SUB = 0b1111, True
    HALT = 0b0000, False
    MUL = 0b0110, True


class AddrMode(IntEnum):
    STRAIGHT_LOAD = 0b000000000000000000000000
    STRAIGHT_ABSOLUTE = 0b000001000000000000000000
    INDIRECT_ABSOLUTE = 0b000010000000000000000000
    ADDR_REFERENCE = 0b000011000000000000000000


code2command = {
    0b0001: Command.ADD,
    0b0010: Command.LD,
    0b0011: Command.INC,
    0b0100: Command.DEC,
    0b0101: Command.CMP,
    0b0110: Command.MUL,
    0b0111: Command.JUMP,
    0b1000: Command.BEQ,
    0b1001: Command.BNE,
    0b1010: Command.BMI,
    0b1011: Command.BPL,
    0b1100: Command.OUT,
    0b1101: Command.COUT,
    0b1110: Command.ST,
    0b1111: Command.SUB,
    0b0000: Command.HALT
}


class HaltProgram(Exception):
    def __init__(self, message="Processor stopped"):
        self.message = message
        super().__init__(self.message)


def twos_complement(number):
    mask = (1 << WordWidth) - 1
    if number < 0:
        number = ((abs(number) ^ mask) + 1)
    return bin(number & mask)
