# Архитектура компьютера. <br >Лабораторная работа №3.

### Выполнил
Стуков Егор Александрович,
P33302

### Вариант

```
asm | acc | harv | hw | instr | binary | stream | port | prob1
```

### Цель
1) Экспериментальное знакомство с устройством процессоров через моделирование
2) Получение опыта работы с компьютерной системой на нескольких уровнях организации.

## Язык программирования
### Структура программы
```
.data
<var_name>: <var_value> ;<comment>
.code
<label>: <operation_mnemonic> <operand> ;<comment>
```
### BNF
```
<operation_no_arg> ::= "INC" 
<operation_one_arg> ::= "JMP" | "LD" | "ADD"
<operation> ::= <operation_no_arg> | <operation_one_arg> <data_value>
<space> ::= " "
<new_line> ::= "\t"
<letter> ::= ([a-z] | [A-Z])+
<space_or_letter> ::= <space> | <letter>
<comment> ::= ; <space_or_letter> <new_line> | "" 
<label> ::= <letter>

<data_value> ::= (<letter>)+ | [0-9]+
<data_line> ::= <label>: <data_value> <comment>

<code_line> ::= <label>: <operation> <comment>

<data_segment> ::= ".data" <new_line> (<data_line>)+
<code_segment> ::= ".code" <new_line> (<code_line>)+ 
 
<program> ::= [<data_segment>] <code_segment>
```

## Организация памяти
### Работа с памятью на уровне языка
- В данной реализации литералы и переменные определяются в сегменте `.data` в следующем формате:
```
.data
    <значение_литерала>
    <название_переменной>: <значение_переменной>
```
- Значение литерала/переменной можно задать в виде числа, символа, ascii-кода символа
- Значение литерала можно также использовать в качестве аргумента команды, указав его через `#`.<br>
Например, команда `ADD #40` добавит к значению аккумулятора значение `40`
```
.code
    start:
    ADD #40
```
- Работая с переменными, можно обратиться к:
  * Значению переменной через `#var_name`
  * Адресу переменной через `var_name`
  * Данным, лежащим по адресу, хранимому в значении переменной через `$var_name`
### Модель памяти процессора
- Память команд и данных разделена (Гарвардская архитектура).
- Линейное адресное пространство
- Реализуются списком строк, в которых каждая строка определяет одну инструкцию / элемент данных. 
Строки имеют бинарное представление.
- И в памяти данных, и в пямяти команд используется машинное слово размером 24 бита.
Подробнее об организации машинного слова в след. разделе
```
     Instruction memory
+------------------------------+
| 00  : jmp n                  |
|             ...              |
| 10  : instruction            | 
| 11  : instruction            |
|             ...              |
| n   : program start          |
|             ...              |
| i   : instruction            |
| i+1 : instruction            |
|             ...              |
+------------------------------+

  Data memory
+------------------------------+
| 00  : variable               |
| 01  : variable               |
|    ...                       |
| i-1 : variable               |
| i   : variable               |
+------------------------------+


```
### Отображения конструкций языка на модель памяти процессора
- При обращении в команде к переменной в инструкцию процессора записывается код операции, код типа адресации и адрес операнда.
- При указании значения в качестве аргумента значение аргумента записывается в 19 младших бит инструкции.

## Система команд
### Особенности процессора
- Машинное слово команды – 24 бит. Беззнаковое. Абсолютная адресация команд.
- Машинное слово данных – 24 бит. Знаковое. Абсолютная адресация данных.
- При обращении к памяти команд инструкция помещается в регистр команд `CR`, аналогично, данные помещаются в регистр данных `DR`
- Ввод-вывод осуществляется через порты, токенизирован, символьный
- Изменение данных осуществляется через АЛУ и аккумулятор `AC`
### Набор инструкций
| Syntax   | Description       |
|:---------|:------------------|
| `ADD M`  | `AC := AC + M`    |
| `SUB M`  | `AC := AC - M`    |
| `INC`    | `AC := AC + 1`    |
| `DEC`    | `AC := AC - 1`    |
| `CMP M`  | `set ZF and NF`   |
| `JUMP M` | `PC := M`         |
| `BEQ M`  | `jump if ZF == 1` |
| `BNE M`  | `jump if ZF == 0` |
| `BMI M`  | `jump if NF == 1` |
| `BPL M`  | `jump if NF == 0` |
| `OUT`    | `out AC`          |
| `COUT`   | `out chr(AC)`     |
| `ST M`   | `MEM(M) := AC`    |
| `LD M`   | `AC := M`         |
| `MUL M`  | `AC := AC * M`    |
| `HALT`   | `stop processor`  |
### Структура машинного слова команды
```
         0000            00    000000000000000000
            ^             ^                     ^
            |             |                     |
  instr. code    addr. mode               operand              
                             
```
 - `instr. code` - код инструкции (подробнее см. ниже)
 - `addr. mode` - режим адресации (прямая загрузка операнда, абсолютная, косвенная)
 - `operand` – содержит адрес / значение при режиме прямой загрузки 
### Кодирование инструкций
| Syntax   | Instruction code |
|:---------|:-----------------|
| `ADD M`  | `0001`           |
| `SUB M`  | `1111`           |
| `INC`    | `0011`           |
| `DEC`    | `0100`           |
| `CMP M`  | `0101`           |
| `JUMP M` | `0111`           |
| `BEQ M`  | `1000`           |
| `BNE M`  | `1001`           |
| `BMI M`  | `1010`           |
| `BPL M`  | `1011`           |
| `OUT`    | `1100`           |
| `COUT`   | `1101`           |
| `ST M`   | `1110`           |
| `LD M`   | `0010`           |
| `MUL M`  | `0110`           |
| `HALT`   | `0000`           |

## Траснлятор
- Интерфейс командной строки: <br>
  `translator.py <input_file> <target_file>"`
- Реализовано в модуле: [translator](./translator.py)

### Особенности работы
* Происходит в 2 этапа:
  1. Составляется словарь меток, содержащих адреса команд, на которые метки указывают. 
  Сегмент данных и комментарии игнорируются
  2. Полное сканирование файла программы
* Комментарии отбрасываются
* Проверка на корректность мнемоник команд
* Термы кодируются в бинарный вид
* Преобразованные команды логируются в виде мнемоник

## Модель процессора

Реализован в двух модулях: [control_unit](./control_unit.py), [datapath](./datapath.py).

### DataPath

<img src="schema/datapath.png" alt="drawing" width="600"/>

Позволяет обращаться к памяти инструкций/данных, сохранять данные, 
а также устанавливать флаги `ZF` и `NF` по значению, хранимому в `AC`

### Control Unit 

<img src="schema/control_unit.png" alt="drawing" width="600"/>

Hardwired.
Моделирование на уровне инструкций.

Достает команду из памяти с помощью DataPath по адресу, который хранится в Program Counter.
<br>
Для адресных команд используется маска, позволяющая определить тип перехода и аргумент.
<br>
После декодирования инструкции происходит её исполнение
<br>
Когда была декодирована команда halt, работа процессора останавливается (реализовано с помощью прерывания)

## Апробация
### Тестирование
Модульные тесты:
 - [translator_test](./translator_test.py). Проверка корректности трансляции программ в машинный код
 - [processor_test](./processor_test.py). Проверка коррекности выполнения программ

Интеграционные тесты:
 - [integration_test](./integration_test.py). Проверка работы модели на алгоритмах `hello`, `cat`, `prob1`

### CI
```
lab3-example:
  stage: test
  image:
    name: python-tools
    entrypoint: [""]
  script:
    - python3-coverage run -m pytest --verbose
    - find . -type f -name "*.py" | xargs -t python3-coverage report
    - find . -type f -name "*.py" | xargs -t pep8 --ignore=E501
    - find . -type f -name "*.py" | xargs -t pylint
```
| ФИО          | алг.  | LoC | code байт | code инстр. | инстр.   | такт. | вариант                                                |
|--------------|-------|-----|-----------|-------------|----------|-------|--------------------------------------------------------|
| Стуков Е. А. | hello | 40  | 81        | 26          | 27       | 80    | asm, acc, harv, hw, instr, binary, stream, port, prob1 |
| Стуков Е. А. | cat   | 31  | 30        | 10          | 148      | 460   | asm, acc, harv, hw, instr, binary, stream, port, prob1 |
| Стуков Е. А. | prob1 | 53  | 105       | 38          | 6006     | 20417 | asm, acc, harv, hw, instr, binary, stream, port, prob1 |
