# pylint: disable=missing-class-docstring
# pylint: disable=missing-function-docstring
# pylint: disable=line-too-long
# pylint: disable=too-many-arguments

"""
Test translator module on three algorithms: cat, hello, prob1
"""

import os.path
import unittest
import tempfile

import translator


class TestTranslator(unittest.TestCase):

    def assert_equal_output(self, source: str, code_target: str, data_target: str,
                            right_code_target, right_data_target):

        translator.main([source, code_target, data_target])

        with open(code_target, encoding="utf-8") as file:
            code_result = file.read()
        with open(right_code_target, encoding="utf-8") as file:
            right_code_result = file.read()
        self.assertEqual(code_result, right_code_result)

        with open(data_target, encoding="utf-8") as file:
            data_result = file.read()
        with open(right_data_target, encoding="utf-8") as file:
            right_data_result = file.read()
        self.assertEqual(data_result, right_data_result)

    def test_cat_translation(self):
        with tempfile.TemporaryDirectory() as tmpdir:
            source: str = "test/in/test_cat.asm"
            right_code_target: str = "test/out/test_cat_code"
            right_data_target: str = "test/out/test_cat_data"

            code_target: str = os.path.join(tmpdir, "cat_code")
            data_target: str = os.path.join(tmpdir, "cat_data")

            self.assert_equal_output(source, code_target, data_target, right_code_target, right_data_target)

    def test_hello_translation(self):
        with tempfile.TemporaryDirectory() as tmpdir:
            source: str = "test/in/test_hello.asm"
            right_code_target: str = "test/out/test_hello_code"
            right_data_target: str = "test/out/test_hello_data"

            code_target: str = os.path.join(tmpdir, "hello_code")
            data_target: str = os.path.join(tmpdir, "hello_data")

            self.assert_equal_output(source, code_target, data_target, right_code_target, right_data_target)

    def test_prob1_translation(self):
        with tempfile.TemporaryDirectory() as tmpdir:
            source: str = "test/in/test_prob1.asm"
            right_code_target: str = "test/out/test_prob1_code"
            right_data_target: str = "test/out/test_prob1_data"

            code_target: str = os.path.join(tmpdir, "prob1_code")
            data_target: str = os.path.join(tmpdir, "prob1_data")

            self.assert_equal_output(source, code_target, data_target, right_code_target, right_data_target)
